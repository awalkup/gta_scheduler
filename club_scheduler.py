#!/usr/bin/python2

#
# Austin Walkup
# Oct 16, 2012
#
# Purpose: find the optimal times to schedule meetings.
#

import os, sys, uuid, csv
import numpy as np
import itertools
import operator

def parse_input(input_schedule):
	'''
	  Reads the input file into a numpy array (matrix), and returns it 
	  with the names and the unames (uuid -> original name) dictionary.
	'''
	temp_list = []
	with open(input_schedule, 'rb') as f:
		csvReader = csv.reader(f, delimiter=',')
		names = csvReader.next()

		unames = {}
		def conv(uname):
			'''
			  Converter function for numpy's loadtxt function.
			'''
			randId = uuid.uuid4().int
			unames[randId] = uname
			return randId
		#end converter
		schedule_mat = np.loadtxt(f, delimiter=',', \
								  usecols=range(len(names)),  \
								  converters={0 : conv} )

	return schedule_mat, names[1:], unames
			
#end parse_input

def find_best_times(schedule_mat, r):
	'''
	  Calculates the best times to meet by determining the largest subset 
	  of participants.
	'''
	unames = schedule_mat[:,:1]
	mat = schedule_mat[:,1:].T
	nrows, ncols = mat.shape

	# get a list of sets along the columns (of unique participants)
	col_sets = [ frozenset( \
				 [unames[j,0] for j in range(ncols) if mat[i,j]] ) \
				 for i in range(nrows) ]

	combIndecies = itertools.combinations(xrange(len(col_sets)), r)

	total_entries = schedule_mat.shape[0]
	results = {}
	for indecies in combIndecies:
		entries = len(frozenset().union(*[col_sets[i] for i in indecies]))
		results[indecies] = (entries/float(total_entries))*100

	sorted_results = sorted(results.iteritems(), key=operator.itemgetter(1))
	return sorted_results[::-1]

def output_best_times(best_times, date_times):
	'''
	  Outputs the best times and their percentage of participants in an
	  easy to read fashion
	'''
	max_width = max(len(x) for x in date_times)+1
	max_width *= 2

	print '{0:{col1}} : {1:{col2}}'.format('DateTime(s)', 'Percent of people', col1=max_width, col2=max_width)
	for indecies_people in best_times:
		indecies  = indecies_people[0]
		people = indecies_people[1]
		time = '/'.join([date_times[i] for i in indecies])
		print '{0:{col1}} : {1:.1f}%'.format(time, people, col1=max_width)
		
#end output_best_times

def main(input_schedule, r):
	schedule_mat, names, unames = parse_input(input_schedule)
	
	best_times = find_best_times(schedule_mat, r)

	output_best_times(best_times, names)
#end main

def usage(arg0):
	'''
	  How to use the program
	'''
	print 'Usage:\n\t', arg0, '<input_schedule.csv>','[number_of_times=2]'
	exit(1)
#end usage

if __name__ == '__main__':
	if len(sys.argv) < 2 or len(sys.argv) > 3:
		usage(sys.argv[0])

	input_schedule = sys.argv[1]
	r = 2
	try:
		r = int(sys.argv[2])
	except:
		pass

	main(input_schedule, r)
